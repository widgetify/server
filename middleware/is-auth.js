const jwt = require('jsonwebtoken');

module.exports = function isAuth (req, res, next) {
    const token = req.headers['auth-token'];
    if(!token || token ==='') {
        req.isAuth = false;
        return next();
    }
    let decodedToken;
    try {
        decodedToken = jwt.verify(token, process.env.TOKEN_SECRET)
    } catch(err) {
        req.isAuth = false;
        return next();
    };
    req.isAuth = true;
    req.user = decodedToken;
    next();
}