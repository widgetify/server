const mongoose = require('mongoose');
const { composeWithMongoose } = require('graphql-compose-mongoose');
const {ScopeTC} = require('./Scope')
const {WidgetTC} = require('./Widget')

const roleSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 4,
        max: 100
    },
    widget: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Widget"
    },
    scopes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Scope"
    }]
})

const Role = mongoose.model('Role', roleSchema);
const RoleTC = composeWithMongoose(Role);

RoleTC.addRelation('widget', {
    resolver: () => WidgetTC.getResolver('findByIds'),
    prepareArgs: {
        _ids: (source) => source.widget
    },
    projection: { widget: true },
})

RoleTC.addRelation('scopes', {
    resolver: () => ScopeTC.getResolver('findByIds'),
    prepareArgs: {
        _ids: (source) => source.widget
    },
    projection: { scopes: true },
})

const roleQuery = {
    roleById: RoleTC.getResolver('findById'),
    roleByIds: RoleTC.getResolver('findByIds'),
    roleOne: RoleTC.getResolver('findOne'),
    roleMany: RoleTC.getResolver('findMany'),
    roleCount: RoleTC.getResolver('count'),
    roleConnection: RoleTC.getResolver('connection'),
    rolePagination: RoleTC.getResolver('pagination'),
}
const roleMutation = {
    roleCreateOne: RoleTC.getResolver('createOne'),
    roleCreateMany: RoleTC.getResolver('createMany'),
    roleUpdateById: RoleTC.getResolver('updateById'),
    roleUpdateOne: RoleTC.getResolver('updateOne'),
    roleUpdateMany: RoleTC.getResolver('updateMany'),
    roleRemoveById: RoleTC.getResolver('removeById'),
    roleRemoveOne: RoleTC.getResolver('removeOne'),
    roleRemoveMany: RoleTC.getResolver('removeMany'),
};

module.exports = {
    Role,
    RoleTC,
    roleQuery,
    roleMutation
}