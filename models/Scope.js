const mongoose = require('mongoose');
const { composeWithMongoose } = require('graphql-compose-mongoose');

const scopeSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 4,
        max: 100
    }
})

const Scope = mongoose.model('Scope', scopeSchema);
const ScopeTC = composeWithMongoose(Scope);

const scopeQuery = {
    scopeById: ScopeTC.getResolver('findById'),
    scopeByIds: ScopeTC.getResolver('findByIds'),
    scopeOne: ScopeTC.getResolver('findOne'),
    scopeMany: ScopeTC.getResolver('findMany'),
    scopeCount: ScopeTC.getResolver('count'),
    scopeConnection: ScopeTC.getResolver('connection'),
    scopePagination: ScopeTC.getResolver('pagination'),
}
const scopeMutation = {
    scopeCreateOne: ScopeTC.getResolver('createOne'),
    scopeCreateMany: ScopeTC.getResolver('createMany'),
    scopeUpdateById: ScopeTC.getResolver('updateById'),
    scopeUpdateOne: ScopeTC.getResolver('updateOne'),
    scopeUpdateMany: ScopeTC.getResolver('updateMany'),
    scopeRemoveById: ScopeTC.getResolver('removeById'),
    scopeRemoveOne: ScopeTC.getResolver('removeOne'),
    scopeRemoveMany: ScopeTC.getResolver('removeMany'),
};

module.exports = {
    Scope,
    ScopeTC,
    scopeQuery,
    scopeMutation
}