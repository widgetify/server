const mongoose = require('mongoose');
const { composeWithMongoose } = require('graphql-compose-mongoose');

const widgetSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 4,
        max: 100
    }
})

const Widget = mongoose.model('Widget', widgetSchema);
const WidgetTC = composeWithMongoose(Widget);

const widgetQuery = {
    widgetById: WidgetTC.getResolver('findById'),
    widgetByIds: WidgetTC.getResolver('findByIds'),
    widgetOne: WidgetTC.getResolver('findOne'),
    widgetMany: WidgetTC.getResolver('findMany'),
    widgetCount: WidgetTC.getResolver('count'),
    widgetConnection: WidgetTC.getResolver('connection'),
    widgetPagination: WidgetTC.getResolver('pagination'),
}
const widgetMutation = {
    widgetCreateOne: WidgetTC.getResolver('createOne'),
    widgetCreateMany: WidgetTC.getResolver('createMany'),
    widgetUpdateById: WidgetTC.getResolver('updateById'),
    widgetUpdateOne: WidgetTC.getResolver('updateOne'),
    widgetUpdateMany: WidgetTC.getResolver('updateMany'),
    widgetRemoveById: WidgetTC.getResolver('removeById'),
    widgetRemoveOne: WidgetTC.getResolver('removeOne'),
    widgetRemoveMany: WidgetTC.getResolver('removeMany'),
};

module.exports = {
    Widget,
    WidgetTC,
    widgetQuery,
    widgetMutation
}