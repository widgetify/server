const mongoose = require('mongoose');
const { composeWithMongoose } = require('graphql-compose-mongoose');
const {RoleTC} = require('./Role')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 4,
        max: 100
    },
    email: {
        type: String,
        required: true,
        min: 6,
        max: 100
    },
    password: {
        type: String,
        required: true,
        min: 10,
        max: 100
    },
    salt: {
        type: String,
        required: true,
    },
    confirmed: {
        type: Boolean,
        default: false,
    },
    date: {
        type: Date,
        default: Date.now
    },
    scopes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Scope"
    }],
    roles: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Role"
    }],
})

const User = mongoose.model('User', userSchema);
const UserTC = composeWithMongoose(User);

UserTC.addRelation('roles', {
    resolver: () => RoleTC.getResolver('findByIds'),
    prepareArgs: {
        _ids: (source) => source.roles,
        skip: null,
        sort: null,
    },
    projection: { roles: true },
})

const mw1 = async (resolve, source, args, context, info) => {
    const resp = await resolve(source, args, context, info);
    //res.status(402).end(); 
    return resp;
  };
  

const userQuery = {
    userById: UserTC.getResolver('findById').withMiddlewares([mw1]),
    userByIds: UserTC.getResolver('findByIds'),
    userOne: UserTC.getResolver('findOne'),
    userMany: UserTC.getResolver('findMany'),
    userCount: UserTC.getResolver('count'),
    userConnection: UserTC.getResolver('connection'),
    userPagination: UserTC.getResolver('pagination'),
  }

  

  const userMutation = {
    userCreateOne: UserTC.getResolver('createOne'),
    userCreateMany: UserTC.getResolver('createMany'),
    userUpdateById: UserTC.getResolver('updateById'),
    userUpdateOne: UserTC.getResolver('updateOne'),
    userUpdateMany: UserTC.getResolver('updateMany'),
    userRemoveById: UserTC.getResolver('removeById'),
    userRemoveOne: UserTC.getResolver('removeOne'),
    userRemoveMany: UserTC.getResolver('removeMany'),
  };

module.exports = {
    User,
    UserTC,
    userQuery,
    userMutation
}