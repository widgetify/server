FROM node:12

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

ENV REACT_FRONTEND_URL=35.195.20.171 DB_CONNECT=mongodb+srv://arjuna-dev:P@ssw0rd@widgetifycluster1-waqe5.gcp.mongodb.net/test?retryWrites=true&w=majority TOKEN_SECRET=VJBDJ&8J134kj_)(&76)1_\|/_2a G_PASS=widgetifunk 

# Bundle app source
COPY . .

EXPOSE 4000
CMD [ "yarn", "start" ]