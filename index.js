const cors = require('cors');
const dotenv = require('dotenv');
const express = require('express');
const app = express();
const authRoute = require('./routes/auth');
const oauthRoute = require('./routes/oauth');
const displayWidgetsRoute = require('./routes/displayWidgets');
const mongoose = require('mongoose');
const graphqlHTTP = require('express-graphql');
//const schema = require('./schema/schema');
const isAuth = require('./middleware/is-auth');
const newSchema = require('./schema/newSchema');
app.use(cors());
//app.options('*', cors());  // enable pre-flight
dotenv.config();

app.use(isAuth)

const graphql = function (req) {
    return {
        schema: newSchema,
        graphiql: true,
        context: {
            req
        }
    }
}

app.use('/graphql',  graphqlHTTP(graphql));

//Connect to db
mongoose.connect(process.env.DB_CONNECT, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    },
    () => console.log("Connected to cloud atlas db"))


//Middleware

app.use(express.json());

//Route middlewares
app.use('/api/user', authRoute);
app.use('/bucketoauth', oauthRoute);
app.use('/api/displayWidgets', displayWidgetsRoute);

app.get('/', (req, res) => res.send('Hello there, node app here!'));

app.listen(4000, () => console.log("Listening on port 4000"));
