//Validation
const Joi = require('@hapi/joi');

const registerValidation = (data) => {
    const schema = Joi.object({
        name: Joi.string()
            .min(4)
            .required(),
        email: Joi.string()
            .email({ minDomainSegments: 2 })
            .min(6)
            .required(),
        password: Joi.string()
            // .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
            .min(10)
            .required()
    })

    const {error, validation} = schema.validate(data)
    console.log('error:', error)
    console.log('validation:', validation)
    return error
}

const loginValidation = (data) => {
    const schema = Joi.object({
        email: Joi.string()
            .email({ minDomainSegments: 2 })
            .min(6)
            .required(),
        password: Joi.string()
            .min(10)
            .required()
    })
    const {error, validation} = schema.validate(data)
    return error
}

const passwordResetValidation = (data) => {
    const schema = Joi.object({
        email: Joi.string()
            .email({ minDomainSegments: 2 })
            .min(6)
            .required()
    })
    const {error, validation} = schema.validate(data)
    return error
}

const newPasswordValidation = (data) => {
    const schema = Joi.object({
        id: Joi.string()
            .required(),
        token: Joi.string()
            .required(),
        password: Joi.string()
            // .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
            .min(10)
            .required()
    })
    const {error, validation} = schema.validate(data)
    return error
}

const verifyEmailValidation = (data) => {
    const schema = Joi.object({
        id: Joi.string()
            .required(),
        token: Joi.string()
            .required(),
    })
    const {error, validation} = schema.validate(data)
    return error
}

module.exports.registerValidation = registerValidation
module.exports.loginValidation = loginValidation
module.exports.passwordResetValidation = passwordResetValidation
module.exports.newPasswordValidation = newPasswordValidation
module.exports.verifyEmailValidation = verifyEmailValidation