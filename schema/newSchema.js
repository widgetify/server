const { SchemaComposer } = require('graphql-compose');
const { userQuery, userMutation } = require('../models/User');
const { roleQuery, roleMutation } = require('../models/Role');
const { scopeQuery, scopeMutation } = require('../models/Scope');
const { widgetQuery, widgetMutation } = require('../models/Widget');
const authOnly = require('../middleware/auth-only');

const capaSchemaComposer = new SchemaComposer();

capaSchemaComposer.Query.addFields(
  {...userQuery, ...authOnly({...roleQuery}), ...scopeQuery, ...widgetQuery}
);
capaSchemaComposer.Mutation.addFields(
  {...userMutation, ...roleMutation, ...scopeMutation, ...widgetMutation}
);
const graphqlSchema = capaSchemaComposer.buildSchema();
module.exports =  graphqlSchema;