const graphql = require('graphql');
const user = require('../models/User');

const { GraphQLObjectType, GraphQLString, GraphQLID, GraphQLInt, GraphQLSchema, GraphQLBoolean, GraphQLNonNull, GraphQLList } = graphql;

const UserType = new GraphQLObjectType({
    name: 'User',
    fields: () => ({
        _id: { type: GraphQLID },
        name: { type: GraphQLString },
        email: { type: GraphQLString },
        password: { type: GraphQLString },
        salt: { type: GraphQLString },
        confirmed: { type: GraphQLString },
        date: { type: GraphQLInt }
    })
})



const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: () => ({
        users: {
            type: GraphQLList(UserType),
            args: { _id: { type: GraphQLID } },
            resolve(parent, args) {
                return user.find({}, function(err, doc){
                    if (err) console.log(err)
                    return doc
                });
            }
        },
        getUser: {
            type: UserType,
            args: { _id: { type: GraphQLID } },
            resolve(parent, args) {
                return user.findById(args._id)
            }
        },
    })
});


const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields:{
        confirmUser: {
            type: UserType,
            args: {
                id: {type: GraphQLNonNull(GraphQLID)},
                confirmed: {type: GraphQLBoolean},
            },
            resolve(parent, args){
                return user.findByIdAndUpdate(args.id, {confirmed: true})
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
})