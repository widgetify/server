const router = require('express').Router();
const verify = require('./verifyToken');


//Using verify middleware to check if user has a jwt

router.get('/', verify, (req,res) => {
// router.get('/', (req,res) => {
    res.json({
        data:{
            message: 'This code should display only for verified users'
        }
    })
})

module.exports = router;