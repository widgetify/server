require('dotenv').config();
const router = require('express').Router();
const { User } = require('../models/User')
const { registerValidation, loginValidation, passwordResetValidation, newPasswordValidation, verifyEmailValidation} = require('../validation')
const scrypt = require('scryptsy')
const jwt = require('jsonwebtoken')
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const cors = require('cors');
const verify = require('./verifyToken');
router.use(cors());

router.post('/signup', async (req, res) => {

    //Use for postman
    // const err = registerValidation(req.body)
    //Use for frontend
    const err = registerValidation(req.body.data)

    const emailExists = await User.findOne({
        email: req.body.email
    });
    if (err) {
        return res.status(400).send(err.details[0].message);
    }
    if (emailExists) {
        return res.status(400).send('Email already exists');
    }

    const bytesalt = crypto.randomBytes(47);
    const salt = bytesalt.toString('Base64');
    const hashedPassword = scrypt(req.body.password, salt, 4096, 8, 1, 64)

    const user = new User({
        name: req.body.name,
        email: req.body.email,
        salt: salt,
        confirmed: false,
        // password: req.body.password
        password: hashedPassword
    });

    try {
        const savedUser = await user.save();

        const payload = {
            _id: savedUser._id
        };

        // Sign token
        jwt.sign(
            payload,
            process.env.TOKEN_SECRET, {
                expiresIn: 60 * 60 * 60 * 24 * 60 // 2 months in seconds
            },
            (err, token) => {
                res.send(token);
            }
        );

        const userEmail = req.body.email

        const secret = process.env.TOKEN_SECRET
        const token2 = jwt.sign(payload,
            secret, {
                expiresIn: 60 * 60 * 60 * 24  // 10 minutes expiration
            })
        //Send email
        // create reusable transporter object using the default SMTP transport
        const transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'widgetify@gmail.com',
                pass: process.env.G_PASS
            }
        });
    
        // send mail with defined transport object
        let info = await transporter.sendMail({
            from: '"Widgetify 👻" <widgetify@gmail.com>', // sender address
            to: userEmail, // list of receivers
            subject: "Activate Your Account", // Subject line
            html: '<p>Follow the link to verify your email. If you didn\'t signup for Widgetify, ignore this email.</p> <a href="http://' + process.env.REACT_FRONTEND_URL + '/verifyemail/' + payload._id + '/' + token2 + '">Activate Account</a>' // html body
        });
    
        console.log("Message sent: %s", info.messageId);

    } catch (err) {
        return res.status(400).send(err);
    }
});

router.post('/verifyemail', async (req, res) => {
    try {
        console.log("req.body", req.body)
        console.log("req.body.id", req.body.id)
        console.log("req.body.token", req.body.token)
        //Use for postman
        // const err = newPasswordValidation(req.body)
        //Use for frontend
        const err = verifyEmailValidation(req.body)

        if (err) {
            res.status(400).send(err.details[0].message);
        } else {

            const token = req.body.token
            const id = req.body.id
            const user = await User.findOne({
                _id: id
            });
            const secret = process.env.TOKEN_SECRET
            const decoded = jwt.decode(token, secret);
            var current_time = Date.now().valueOf() / 1000;
            
            if (!user) {
                console.log("return res.status(400).send('User not found 🤔');")
                return res.status(400).send('User not found 🤔');
            }

            try {
                jwt.verify(token, secret);
            } catch (err) {
                console.log("return res.status(400).send('Invalid token 🤕');")
                return res.status(400).send('Invalid token 🤕');
            }
            
            if (decoded.exp < current_time) {
                console.log("return res.status(400).send('Token expired 😅');")
                return res.status(400).send('Token expired 😅');
            }

            var myquery = {
                _id: id
            };
            var newvalues = {
                $set: {
                    password: user.password,
                    name: user.password,
                    email: user.email,
                    salt: user.salt,
                    confirmed: true
                }
            };

            await User.updateOne(myquery, newvalues, function (err, res) {
                if (err) throw err;
                console.log("1 document updated");
            });
            console.log('confirmed:',user.confirmed)

            const payload = {
                _id: user._id
            };


            // Sign token
            jwt.sign(
                payload,
                process.env.TOKEN_SECRET, {
                    expiresIn: 60 * 60 * 60 * 24 * 60 // 2 months in seconds
                },
                (err, token) => {
                    return res.send(token);
                }
            );
        }
    } catch (error) {
        return res.status(400).send("err0r" + error);
    }
});

router.post('/login', async (req, res) => {
    try {
        // Use for postman
        // const err = loginValidation(req.body)
        // Use for frontend
        const err = loginValidation(req.body.data)
        console.log(req.body)
        const user = await User.findOne({
            email: req.body.email
        });

        if (err) {
            res.status(400).send(err.details[0].message);
        } else if (!user) {
            res.status(400).send('Email doesn\'t exist');
        } else {

            const enteredHashedPassword = scrypt(req.body.password, user.salt, 4096, 8, 1, 64)

            // const compareHashes = req.body.password == user.password
            const compareHashes = enteredHashedPassword == user.password

            if (!compareHashes) {
                return res.status(400).send('Incorrect password');
            } else {

                const payload = {
                    _id: user._id
                };

                const token = jwt.sign(payload,
                    process.env.TOKEN_SECRET, {
                        //seconds in a min, times min in an hour, times...
                        //two month expiration
                        expiresIn: 60 * 60 * 60 * 24 * 60
                    })
                return res.send(token)
            }
        }
    } catch (error) {
        console.log('error:', error)
    }
});

router.post('/passwordreset', async (req, res) => {
    try {
        //Use for postman
        // const err = passwordResetValidation(req.body)
        //Use for frontend
        const err = passwordResetValidation(req.body.data)

        const user = await User.findOne({
            email: req.body.email
        });

        if (err) {
            res.status(400).send(err.details[0].message);
        } else if (!user) {
            res.status(400).send('Email doesn\'t exist');
        } else {

            const payload = {
                _id: user._id
            };
            const secret = user.password
            const token = jwt.sign(payload,
                secret, {
                    expiresIn: 60 * 60 * 10 // 10 minutes expiration
                })

            //Send email
            // create reusable transporter object using the default SMTP transport
            const transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'widgetify@gmail.com',
                    pass: process.env.G_PASS
                }
            });

            const userEmail = req.body.email

            // send mail with defined transport object
            let info = await transporter.sendMail({
                from: '"👻" <widgetify@gmail.com>', // sender address
                to: userEmail, // list of receivers
                subject: "Password Reset", // Subject line
                html: '<p>Follow the link to reset your Widgetify password. If you weren\'t trying to reset your password, ignore this email.</p> <a href="http://' + process.env.FRONTEND_URL + '/newpassword/' + payload._id + '/' + token + '">Reset password</a>' // html body
            });

            console.log("Message sent: %s", info.messageId);
            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
        }

        return res.status(200).send("OK");

    } catch (error) {
        return res.status(400).send("err0r" + error);
    }

});

router.post('/newpassword', async (req, res) => {
    try {
        console.log("req.body.data", req.body.data)
        //Use for postman
        // const err = newPasswordValidation(req.body)
        //Use for frontend
        const err = newPasswordValidation(req.body.data)

        if (err) {
            res.status(400).send(err.details[0].message);
        } else {

            const token = req.body.token
            const id = req.body.id
            const user = await User.findOne({
                _id: id
            });
            const secret = user.password
            const decoded = jwt.decode(token, secret);
            var current_time = Date.now().valueOf() / 1000;

            if (!user) {
                return res.status(400).send('Something went wrong');
            }

            try {
                jwt.verify(token, secret);
            } catch (err) {
                return res.status(400).send('Something went wrong 🤔');
            }

            if (decoded.exp < current_time) {
                return res.status(400).send('Token expired 🤕');
            }

            const hashedPassword = scrypt(req.body.password, user.salt, 4096, 8, 1, 64)
            console.log('req.body.password:', req.body.password)
            console.log('user.password1:', user.password)
            var myquery = {
                _id: id
            };
            var newvalues = {
                $set: {
                    password: hashedPassword,
                    name: user.password,
                    email: user.email,
                    salt: user.salt
                }
            };
            await User.updateOne(myquery, newvalues, function (err, res) {
                if (err) throw err;
                console.log("1 document updated");
            });
            console.log('user.password2:', user.password)

            const payload = {
                _id: user._id
            };


            // Sign token
            jwt.sign(
                payload,
                process.env.TOKEN_SECRET, {
                    expiresIn: 60 * 60 * 60 * 24 * 60 // 2 months in seconds
                },
                (err, token) => {
                    return res.send(token);
                }
            );
        }
    } catch (error) {
        return res.status(400).send("err0r" + error);
    }

});

router.get('/checkauth', verify, async (req, res) => {
    res.status(200).send({msg: "authenticated", data: req.user});
})


module.exports = router;