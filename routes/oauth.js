const router = require('express').Router();
const qs = require('qs');
var request = require('request');
const cors = require('cors');
router.use(cors());
//router.options('*', cors());  // enable pre-flight


var data = { 
    'client_secret': 'gcf77kR2bBnZlIBWGCZa6fjU',
    'grant_type': 'refresh_token',
    'refresh_token': '1//042RbQhS8JKizCgYIARAAGAQSNgF-L9IrUi10dBATlqFVOcv1TSdKRjCcZfy_FLHsqAW2zf7h70Fc79H6iJuyLbq25x0HxTTruA',
    'client_id': '607566240787-ancmgnfhssdih1gaf9ml2sung637g7la.apps.googleusercontent.com',
}
var formData = qs.stringify(data);
var contentLength = formData.length;

router.get('/', async (req, res) => {
    request({
        url: 'https://oauth2.googleapis.com/token', 
        method: 'POST',
        headers: {
            'Content-length': contentLength,
            'content-type': 'application/x-www-form-urlencoded',
            'user-agent': 'google-oauth-playground',
        },
        body: formData,
            }, function(error, response, body){
            // console.log('body:', body);
            res.send(body)
    });
    
});

module.exports = router;